import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RequestsRoutingModule } from './requests-routing.module';
import { RequestsListComponent } from './requests-list/requests-list.component';
import { RequestsService } from '../commons/requests/requests.service';


@NgModule({
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, RequestsRoutingModule ],
    declarations: [ RequestsListComponent ],
    exports: [ RequestsListComponent ],
    providers: [ RequestsService ]
})
export class RequestsModule { }
