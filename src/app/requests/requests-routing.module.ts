import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestsListComponent } from './requests-list/requests-list.component';

const routes: Routes = [
    {
        path: '', redirectTo: 'list'
    },
    {
        path: 'list',
        component: RequestsListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestsRoutingModule { }
