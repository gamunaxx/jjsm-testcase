import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RequestsListComponent } from './requests-list.component';
import { HomeComponent } from '../../home/home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RequestsService } from '../../commons/requests/requests.service';
import { routes } from './../../app.routes';
import { Router } from '@angular/router';


describe('@RequestListComponent', () => {
  let component: RequestsListComponent;
  let fixture: ComponentFixture<RequestsListComponent>;
  let router: Router;
  let requestsService: RequestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsListComponent, HomeComponent ],
      imports: [ 
        HttpClientTestingModule,
        SharedModule,
        RouterTestingModule.withRoutes(routes) 
      ],
      providers: [ RequestsService ]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestsListComponent);
    component = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    requestsService = fixture.debugElement.injector.get(RequestsService);
    router.initialNavigation();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => {
      spyOn(component, 'initialView');
      spyOn(component, 'getRequests');
    });

    it('should call initialView', () => {
      component.ngOnInit();
      expect(component.initialView).toHaveBeenCalled();
    });

    it('should call getRequests', ( )=> {
      component.ngOnInit();
      expect(component.getRequests).toHaveBeenCalled();
    });
  });

  describe('#initialView', () => {

    beforeEach(() => {
      let store = {};
      spyOn(localStorage, 'removeItem').and.callFake((value) => {
        if (value === 'idField') {
          return store[value];
        }
      })
    });
     
    it('should set statuFiltro', () => {
      const statuFiltro = 'all';
      component.initialView();
      expect(component.statusFiltro).toBe(statuFiltro);
    });

    it('should remove item', () => {
      expect(localStorage.removeItem('idField')).toBeUndefined();
    });



  });
});
