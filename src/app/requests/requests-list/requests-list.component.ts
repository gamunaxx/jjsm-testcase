import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { RequestsService } from '../../commons/requests/requests.service';
import { Requests } from '../../commons/requests/requests.entity';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class RequestsListComponent implements OnInit {
  statusFiltro: string;
  requestsSubscription: Subscription;
  requestsResult: Array<Requests>;
  requestsFiltered: Array<Requests>;

  constructor(
    private requestsService: RequestsService
  ) { }

  ngOnInit() {
    this.initialView();
    this.getRequests();
  }

  initialView() {
    localStorage.removeItem('idField');
    this.statusFiltro = 'all';
  }

  getstatus(status: string) {
    this.statusFiltro = status;
    this.getFilterStatus(status);
  }

  getRequests() {
    this.requestsSubscription = this.requestsService.getRequests()
      .subscribe((res: any) => {
        this.requestsResult = res.response;
        this.getFilterStatus(this.statusFiltro || 'all');
      });
  }

  getFilterStatus(status: string) {
    this.requestsFiltered = this.requestsResult.filter((item) => {
      return item.status === status || status === 'all';
    });
  }

  ngOnDestroy(): void {
    if (this.requestsSubscription) {
      this.requestsSubscription.unsubscribe();
    }
  }

}
