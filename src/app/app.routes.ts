import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';

export const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent
    },
    {
        path: 'manage',
        loadChildren: './manage-data/manage-data.module#ManageDataModule'
    },
    {
        path: 'requests',
        loadChildren: './requests/requests.module#RequestsModule'
    },
    { path: '404', component: NotFoundComponent },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', redirectTo: '/404' }
];
