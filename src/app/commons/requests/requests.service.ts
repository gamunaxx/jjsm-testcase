import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Requests } from './requests.entity';

@Injectable()
export class RequestsService {
    constructor(private http: HttpClient) {}

    getRequests(): Observable<Requests> {
        return <Observable<Requests>>this.http.get(`/api/requests/list`);
    }
}
