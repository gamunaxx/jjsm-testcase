export interface Requests extends Object {
   date: string;
   reason: string;
   status: string;
}
