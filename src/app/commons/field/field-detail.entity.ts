export interface FieldDetails extends Object {
    idField: string;
    keyName: string;
    description: string;
    type: string;
    typeName: string;
    sensitivity: boolean;
    posiblevalues: [
       {
            id: string,
            value: string,
            description: string
        },
        {
            id: string,
            value: string,
            description: string
        }
    ];
}
