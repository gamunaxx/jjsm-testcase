import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Field } from './field.entity';
import { FieldDetails } from './field-detail.entity';

@Injectable()
export class FieldService {
    constructor(private http: HttpClient) {}

    getFields(): Observable<Field> {
        return <Observable<Field>>this.http.get(`/api/fields/list`);
    }

    getFieldsDetail(id: string): Observable<FieldDetails> {
        return <Observable<FieldDetails>> this.http.get(`/api/field-detail/${id}`);
    }

    postFieldsDetail(fieldDetailData: any): Observable<any> {
        return <Observable<any>>this.http.post('/api/field-detail', fieldDetailData);
    }


}
