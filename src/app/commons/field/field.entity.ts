export interface Field extends Object {
    id: string;
    keyName: string;
}
