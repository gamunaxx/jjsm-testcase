import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { InformationComponent } from './information.component';
import { HomeComponent } from '../../home/home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { routes } from './../../app.routes';
import { FieldService } from '../../commons/field/field.service';
import { FieldDetails } from '../../commons/field/field-detail.entity';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';

describe('@InformationComponent', () => {
  let component: InformationComponent;
  let fixture: ComponentFixture<InformationComponent>;
  let router: Router;
  let fieldService: FieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InformationComponent, HomeComponent ],
      imports: [
        HttpClientTestingModule,
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [FieldService]
    }).compileComponents();

    fixture = TestBed.createComponent(InformationComponent);
    component = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    fieldService = fixture.debugElement.injector.get(FieldService);
    router.initialNavigation();
  });


  describe('#getInfo', () => {
    const mockEvent: Event = <Event><any> {
      id: '1'
    };

    it('should get getInfo', fakeAsync(() => {
      const response = {
        response: [
          {
          'idField': '1',
          'description': 'Contains the primary key used to identify a user of the system',
          'keyName': 'user_id',
          'type': '1',
          'typeName': 'Integer',
          'sensitivity': true,
          'posiblevalues': [
              {
                'id': '1',
                'value': 'null',
                'description': 'value when user is not found'
              },
              {
                'id': '2',
                'value': '{integer}',
                'description': 'ID key of user'
              }
            ]
          }
        ]
      };
      spyOn(fieldService, 'getFieldsDetail').and.returnValue(of(response));
      component.getInfo(mockEvent);
      tick();
    }));
    
  });

  describe('#editar', () => {
    it('should navigate to manage/edit', () => {
      spyOn(router, 'navigate');
      component.editar();
      expect(router.navigate).toHaveBeenCalledWith(['manage/edit']);
    });
  });
});
