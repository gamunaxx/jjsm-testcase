import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { FieldService } from '../../commons/field/field.service';
import { FieldDetails } from '../../commons/field/field-detail.entity';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit, OnDestroy {
  fieldDetailsubscription: Subscription;
  error: string;
  fieldDetailResult: FieldDetails;
  keyName: string;
  description: string;
  typeName: string;
  sensitivity: boolean;
  idField: string;
  prueba = '';

  constructor(
    private router: Router,
    private fieldService: FieldService
  ) {
  }

  ngOnInit() {
    this.prueba = 'hola';
  }

  getInfo(event) {
    this.fieldDetailsubscription = this.fieldService.getFieldsDetail(event.id)
      .subscribe((res: any) => {
        this.fieldDetailResult = res.response;
        localStorage.setItem('fieldDetail', JSON.stringify(this.fieldDetailResult));
        this.keyName = this.fieldDetailResult['keyName'];
        this.description = this.fieldDetailResult['description'];
        this.typeName = this.fieldDetailResult['typeName'];
        this.sensitivity = this.fieldDetailResult['sensitivity'];
      });
  }

  editar() {
    this.router.navigate(['manage/edit']);
  }

  ngOnDestroy() {
    if (this.fieldDetailsubscription) {
      this.fieldDetailsubscription.unsubscribe();
    }
  }

}
