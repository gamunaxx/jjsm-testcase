import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FieldDetails } from '../../commons/field/field-detail.entity';
import { ISubscription } from 'rxjs/Subscription';
import { FieldService } from '../../commons/field/field.service';
declare var $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  editForm: FormGroup;
  fieldKeyName: string;
  field: any;
  keyName: FormControl;
  description: FormControl;
  checkSensitivity: FormControl;
  type: FormControl;
  postFieldSubscription: ISubscription;


  fieldDetailRequest: FieldDetails;

  types = [
    { id: '1', name: 'Integer' },
    { id: '2', name: 'Numeric' },
    { id: '3', name: 'Varchar' },
    { id: '4', name: 'Char' },
    { id: '5', name: 'Date' }
  ];

  selectedValue = null;

  constructor(
    private fieldService: FieldService
  ) { }

  ngOnInit() {
    this.fieldDetailRequest = JSON.parse(localStorage.getItem('fieldDetail')) || {};
    this.setFormEdit();
    $('#success-alert').hide();
  }

  setFormEdit() {
    this.keyName = new FormControl(
      this.fieldDetailRequest.keyName || '',
       [Validators.required, Validators.maxLength(40)]
    );

    this.description = new FormControl(
      this.fieldDetailRequest.description || '', [Validators.required]
    );
    this.checkSensitivity = new FormControl(
      this.fieldDetailRequest.sensitivity
    );

    this.type = new FormControl(
      this.fieldDetailRequest.type || ''
    );

    if (this.keyName.invalid) {
      this.keyName.setValue('');
    }
    if (this.description.invalid) {
      this.description.setValue('');
    }

    this.editForm = new FormGroup({
      keyName: this.keyName,
      description: this.description,
      type: this.type,
      checkSensitivity: this.checkSensitivity
    });
  }

  grabar() {
    const fieldData = {
      keyName: this.keyName.value,
      description: this.description.value,
      type: this.type.value,
      sensitivity: this.checkSensitivity.value
    };

    this.postFieldSubscription = this.fieldService.postFieldsDetail(fieldData).subscribe(() => {
      $('#success-alert').fadeTo(2000, 500).slideUp(500, function () {
        $('#success-alert').slideUp(500);
      });
    });
  }

}
