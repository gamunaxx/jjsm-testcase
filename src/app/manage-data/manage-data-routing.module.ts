import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformationComponent } from './information/information.component';
import { EditComponent } from './edit/edit.component';


const routes: Routes = [
    {
        path: '', redirectTo: 'information'
    },
    {
        path: 'information',
        component: InformationComponent
    },
    {
        path: 'edit',
        component: EditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageDataRoutingModule {}
