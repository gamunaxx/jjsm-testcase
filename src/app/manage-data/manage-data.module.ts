import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { InformationComponent } from './information/information.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../shared/shared.module';
import { ManageDataRoutingModule } from './manage-data-routing.module';
import { FieldService } from '../commons/field/field.service';

@NgModule({
    imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, ManageDataRoutingModule],
    declarations: [InformationComponent, EditComponent],
    exports: [InformationComponent],
    providers: [ FieldService ]
})
export class ManageDataModule { }
