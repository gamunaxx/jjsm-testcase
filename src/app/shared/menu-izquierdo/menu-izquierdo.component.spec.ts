import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MenuIzquierdoComponent } from './menu-izquierdo.component';
import { HomeComponent } from '../../home/home.component';
import { NotFoundComponent } from '../../shared/not-found/not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { routes } from './../../app.routes';
import { FieldService } from '../../commons/field/field.service';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';

describe('@MenuIzquierdoComponent', () => {
  let component: MenuIzquierdoComponent;
  let fixture: ComponentFixture<MenuIzquierdoComponent>;
  let router: Router;
  let fieldService: FieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MenuIzquierdoComponent, HomeComponent, NotFoundComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [FieldService]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuIzquierdoComponent);
    component = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    fieldService = fixture.debugElement.injector.get(FieldService);
    router.initialNavigation();
  });

  afterEach(() => {
    component.ngOnDestroy();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => {
      spyOn(component, 'initialView');
      spyOn(component, 'getFields');
      spyOn(component, 'getSelectedItem');
    });

    it('shoudl call initialView, getFields and getSelectedItem', () => {
      component.idField = null;
      component.idFieldLocal = null;
      component.ngOnInit();

      expect(component.initialView).toHaveBeenCalled();
      expect(component.getFields).toHaveBeenCalled();
      expect(component.getSelectedItem).toHaveBeenCalled();
    });
  });

  describe('#initialView', () => {
    const fieldList = null;
    const idFieldLocal = null;

    beforeEach(() => {
      spyOn(component, 'getFields');
      spyOn(component, 'getSelectedItem');
      spyOn(localStorage, 'getItem').and.callFake((value) => {
        if (value === 'idField') { return idFieldLocal; }
        if (value === 'fieldList') { return JSON.stringify(fieldList); } else { return null; }
      });
    });

    it('should set fieldList object from Localstorage', () => {
      fixture.detectChanges();
      expect(localStorage.getItem).toHaveBeenCalledWith('fieldList');
      expect(component.fieldList).toEqual(fieldList);
    });

    it('should set idField string from Localstorage', () => {
      fixture.detectChanges();
      expect(localStorage.getItem).toHaveBeenCalledWith('idField');

    });
  });

  describe('#getFields', () => {
    it('should get getFields', fakeAsync(() => {
      const response = {
          response: [
          {
            'id': '1',
            'keyName': 'item_id'
          }
        ]
      };
      spyOn(fieldService, 'getFields').and.returnValue(of(response));

      component.getFields();
      tick();
      expect(component.fieldList.length).toBe(1);
    }));
  });

  describe('#getSelectedItem', () => {
    it('should set idField and set data to localStorage', () => {
      spyOn(localStorage, 'setItem');
      const id = '1';
      component.idField = '1';
      component.getSelectedItem(id);
      expect(localStorage.setItem).toHaveBeenCalledWith( 'idField', id);
      expect(component.idField).toBe(id);
      
    });
  });

});
