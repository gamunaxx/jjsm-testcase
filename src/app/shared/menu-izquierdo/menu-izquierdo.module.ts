import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MenuIzquierdoComponent } from './menu-izquierdo.component';
import { FieldService } from '../../commons/field/field.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [MenuIzquierdoComponent],
    exports: [MenuIzquierdoComponent],
    providers: [FieldService ]
})
export class MenuIzquierdoModule { }
