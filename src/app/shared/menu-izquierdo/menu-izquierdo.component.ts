import { Component, OnInit, EventEmitter, Output, OnDestroy  } from '@angular/core';

import { Field } from '../../commons/field/field.entity';
import { FieldService } from '../../commons/field/field.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-menu-izquierdo',
  templateUrl: './menu-izquierdo.component.html',
  styleUrls: ['./menu-izquierdo.component.scss']
})
export class MenuIzquierdoComponent implements OnInit, OnDestroy {
  @Output() obtenerId = new EventEmitter();
  fieldList: Field[];
  idFieldLocal: string;
  idField: string;

  private fieldSubscription: ISubscription;

  constructor(
    private fieldService: FieldService,
  ) { }

  ngOnInit() {
   this.initialView();
    this.getFields();
    this.getSelectedItem(this.idField || this.idFieldLocal || '1');
  }

  initialView() {
    this.fieldList = JSON.parse(localStorage.getItem('fieldList')) as Field[];
    this.idFieldLocal = localStorage.getItem('idField');
  }

  getFields() {
    if (!this.fieldList) {
      this.fieldSubscription = this.fieldService.getFields()
        .subscribe((fieldResponse: any) => {
          this.fieldList = fieldResponse.response;
          localStorage.setItem('fieldList', JSON.stringify(this.fieldList));
        });
    }
  }

  getSelectedItem(id: string) {
    this.obtenerId.emit({id: id});
    localStorage.setItem('idField', id);
    this.idField = id;
  }

  ngOnDestroy() {
    if (this.fieldSubscription) {
      this.fieldSubscription.unsubscribe();
    }
  }

}
