import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { MenuIzquierdoModule } from './menu-izquierdo/menu-izquierdo.module';

@NgModule({
    imports: [ CommonModule, RouterModule ],
    declarations: [
        NotFoundComponent
    ],
    exports: [
        CommonModule,
        NotFoundComponent,
        MenuIzquierdoModule
    ]
})
export class SharedModule { }
