'use strict';

const express = require('express');
const morgan = require('morgan');
var cors = require('cors');

const PORT = 3000;

const fieldRouter = require('./routes/field/field.router');
const requestsRouter = require('./routes/requests/requests.router');

const app = express();

app.use(morgan(process.env.NODE_ENV === 'development' ? 'dev' : 'common', {
    skip: () => process.env.NODE_ENV === 'test'
}));

app.use(express.json());

app.use(cors());

app.use(fieldRouter);
app.use(requestsRouter);

app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });
});

app.listen(PORT, function () {
    console.info(`Server listening on ${this.address().port}`);
}).on('error', err => {
    console.error(err);
});