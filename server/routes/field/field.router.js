'use strict';

const express = require('express');
const router = express.Router();

const { dataSuccess } = require('./field.data');
const { fieldDataSuccess } = require('./field-application.data');

const { fieldUserId, fieldItemId, fieldUserEmail, fielditemViews, fieldItemFavorites } = require('./field-detail.data');
const delayTime = 0;

router.get('/api/fields/list', (req, res) => {
    let status = 200,
        data = dataSuccess;
    setTimeout(() => res.status(status).send(data), delayTime);
});

router.get('/api/field-detail/:id', (req, res) => {
    let id = req.params.id;
    let status = 200,
        data = fieldUserId;

    switch (id) {
        case '1':
            data = fieldUserId;
            break;
        case '2':
            data = fieldItemId;
            break;
        case '3':
            data = fieldUserEmail;
            break;
        case '4':
            data = fielditemViews;
            break;
        case '5':
            data = fieldItemFavorites;
            break;
        default:
            data = fieldItemId;
            break;
    }

    setTimeout(() => res.status(status).send(data), delayTime);
});

router.post('/api/fields/list', (req, res) => {
    const field = req.body.field;    
    let status = 200,
        data = fieldDataSuccess;
    setTimeout(() => res.status(status).send(data), delayTime);
});

router.post('/api/field-detail', (req, res) => {
    const fieldDetail = req.body.fieldDetailData;
    console.log(fieldDetail);
    let status = 200,
        data = fieldDataSuccess;
    setTimeout(() => res.status(status).send(data), delayTime);
})




module.exports = router;