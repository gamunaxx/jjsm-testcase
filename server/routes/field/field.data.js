module.exports = {
    dataSuccess: {
       "response": [
           {
               "id": "1",
               "keyName": "user_id"               
           },
           {
               "id": "2",
               "keyName": "item_id"               
           },
           {
               "id": "3",
               "keyName": "user_email"               
           },
           {
               "id": "4",
               "keyName": "item_views"               
           },
           {
               "id": "5",
               "keyName": "item_favorites"               
           },           
       ],
       "errors": []
    }
}