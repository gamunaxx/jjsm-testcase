module.exports = {
        fieldUserId: {
            "response": {
                "idField": "1",
                "keyName": "user_id",
                "description": "Contains the primary key used to identify a user of the system",
                "type": "1",
                "typeName": "Integer",
                "sensitivity": true,
                "posiblevalues": [
                    {
                        "id": "1",
                        "value": "null",
                        "description": "value when user is not found"
                    },
                    {
                        "id": "2",
                        "value": "{integer}",
                        "description": "ID key of user"
                    }
                ]
            }
        },
        fieldItemId: {
            "response": {
                "idField": "2",
                "keyName": "item_id",
                "description": "Contains the primary key used to identify a user of the system",
                "type": "2",
                "typeName": "Numeric",
                "sensitivity": true,
                "posiblevalues": [
                    {
                        "id": "1",
                        "value": "null",
                        "description": "value when user is not found"
                    },
                    {
                        "id": "2",
                        "value": "{integer}",
                        "description": "ID key of user"
                    }
                ]
            }
            
        },
        fieldUserEmail: {   
            "response": {
                "idField": "3",
                "keyName": "user_email",
                "description": "Contains the primary key used to identify a user of the system",
                "type": "3",
                "typeName": "Varchar",
                "sensitivity": true,
                "posiblevalues": [
                    {
                        "id": "1",
                        "value": "null",
                        "description": "value when user is not found"
                    },
                    {
                        "id": "2",
                        "value": "{integer}",
                        "description": "ID key of user"
                    }
                ]
            }         
            
        },
        fielditemViews: {            
            "response": {
                "idField": "4",
                "keyName": "item_views",
                "description": "Contains the primary key used to identify a user of the system",
                "type": "4",
                "typeName":"char",
                "sensitivity": false,
                "posiblevalues": [
                    {
                        "id": "1",
                        "value": "null",
                        "description": "value when user is not found"
                    },
                    {
                        "id": "2",
                        "value": "{integer}",
                        "description": "ID key of user"
                    }
                ]
            }
        },
        fieldItemFavorites: {   
            "response": {
                "idField": "5",
                "keyName": "item_favorites",
                "description": "Contains the primary key used to identify a user of the system",
                "type": "5",
                "typeName": "Date",
                "sensitivity": true,
                "posiblevalues": [
                    {
                        "id": "1",
                        "value": "null",
                        "description": "value when user is not found"
                    },
                    {
                        "id": "2",
                        "value": "{integer}",
                        "description": "ID key of user"
                    }
                ]
            }         
            
        }
}      


