module.exports = {
    requestsSuccess: {
        "response": [
            {
                "date": "2017/09/21",
                "reason": "Data science algorithms",
                "status": "Pending"
            },
            {
                "date": "2017/08/13",
                "reason": "Stakeholder dashboards",
                "status": "Approved"
            },
            {
                "date": "2017/07/03",
                "reason": "Email blast",
                "status": "Denied"
            },
            {
                "date": "2017/02/09",
                "reason": "Investigation",
                "status": "Approved"
            },
            {
                "date": "2017/09/21",
                "reason": "Data science algorithms",
                "status": "Pending"
            },
            {
                "date": "2017/08/13",
                "reason": "Skateholder dashboards",
                "status": "Approved"
            },
            {
                "date": "2017/07/03",
                "reason": "Email blast",
                "status": "Denied"
            },
            {
                "date": "2017/02/09",
                "reason": "Investigation",
                "status": "Approved"
            },
            {
                "date": "2017/09/21",
                "reason": "Data science algorithms",
                "status": "Pending"
            },
            {
                "date": "2017/08/13",
                "reason": "Skateholder dashboards",
                "status": "Approved"
            },
            {
                "date": "2017/07/03",
                "reason": "Email blast",
                "status": "Denied"
            },
            {
                "date": "2017/02/09",
                "reason": "Investigation",
                "status": "Approved"
            }
        ]
    }
}