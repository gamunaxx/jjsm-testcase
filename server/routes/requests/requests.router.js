'use strict';

const express = require('express');
const router = express.Router();

const { requestsSuccess } = require ('./requests.data');
const delayTime = 0;

router.get('/api/requests/list', (req, res) => {
    let status = 200,
        data = requestsSuccess;
    setTimeout(() => res.status(status).send(data), delayTime);
});

module.exports = router;